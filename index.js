//alert("Hello, B273!")

let students = ["John", "Joe", "Jane", "Jessie"];
console.log(students);

// Review

// 1. What array method can we use to add an item at the end of the array?

students.push("Marina");
console.log(students);

// 2. Add an item at the beginning of the array?

students.unshift("Minty");
console.log(students);

// 3. Deleting an item at the end of the array?

students.pop();
console.log(students);

// 4. Deleting an item at the beginning of the array?

students.shift();
console.log(students);

// 5. What is the difference between splice and slice method?
	// .slice() - (Non-Mutator Methods) - copies a portion from starting index and returns a new array from it

	// .splice() - (Mutator Methods) - removes and adds items from a starting index

// Iterator Methods - loop over the items of array
	/*
		map() - loops over items in an array and repeats a user-defined function AND returns a new array

		forEach() - loops over items in an array and repeats a user-defined function
		
		every() - loops and checks if all the items satisfy a given condition AND returns boolean result (T/F)
	*/

/*
	Mini-Activity:
		Using forEach, check if every item in the array is divisible by 5. If they are, log in the console "<num> is divisible by 5", if not, log false
*/

let arrNum = [15, 20, 23, 30, 37];

/*
	arrNum.forEach(function(i) {
		if(i % 5 === 0) {
			console.log(`${i} is divisible by 5`)
		}
		else {
			console.log(false);
		}
	});
*/

arrNum.forEach(num => {
	if(num % 5 === 0) {
		console.log(`${num} is divisible by 5`);
	}
	else {
		console.log(false);
	}
});

// Math Object

// Javascript Math Object - allows us to perform mathematical tasks on numbers
// ALL methods and properties can be used without creating a Math object first


// Mathematical Constants

// 8 pre-defined properties which can be called via the syntax Math.property

// case-sensitive
console.log(Math);
console.log(Math.E);
console.log(Math.PI);
console.log(Math.SQRT2); 
console.log(Math.SQRT1_2);
console.log(Math.LN2);
console.log(Math.LN10);
console.log(Math.LOG2E);
console.log(Math.LOG10E);

// Methods for rounding of a number to an integer
console.log(Math.round(Math.PI)); // rounds off
console.log(Math.ceil(Math.PI)); // rounds up
console.log(Math.floor(Math.PI)); // rounds down 
console.log(Math.trunc(Math.PI)); // returns the integer of the number - ES6 update

// Square root of a number
console.log(Math.sqrt(3)); 

// Lowest value in the list of arguments
let numVar = -100;

console.log(Math.min(-1, -2, -4, 0, 1, 2, -3, numVar)); // -100

// Highest value in the list of arguments
console.log(Math.max(-1, -2, -4, 0, 1, 2, -3, numVar)); // 2

// Exponent Operator ES6 update
const myNumber = 8 ** 5;
console.log(myNumber);

// Math.pow(x, y) return the value of x to the power of y
const myNumber2 = Math.pow(8, 5);
console.log(myNumber2);

// Primitive Data Types
let name = "Mario";
console.log(name.toUpperCase()); // object method


